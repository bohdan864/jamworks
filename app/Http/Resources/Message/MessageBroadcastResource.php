<?php

namespace App\Http\Resources\Message;

use App\Http\Resources\User\UserResource;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class MessageBroadcastResource extends JsonResource
{
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->resource->id,
            'chat_id' => $this->resource->chat_id,
            'user' => UserResource::make($this->resource->user)->resolve(),
            'body' => $this->resource->body,
            'time' => $this->resource->time,
            'is_owner' => false,
        ];
    }
}
