<?php

namespace App\Http\Resources\Chat;

use App\Http\Resources\Message\MessageResource;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ChatResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->resource->id,
            'title' => $this->resource->title,
            'users' => $this->resource->users,
            'last_message' => $this->resource->lastMessage ? MessageResource::make($this->resource->lastMessage)->resolve() : null,
            'messages' => $this->resource->messages ? MessageResource::collection($this->resource->messages)->resolve() : null,
            'unreadable_count' => $this->resource->unreadable_message_statuses_count
        ];
    }
}
