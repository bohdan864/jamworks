<?php

namespace App\Http\Controllers;

use App\Dto\Message\StoreMessageDto;
use App\Http\Requests\Message\StoreRequest;
use App\Http\Resources\Message\MessageResource;
use App\Services\Facades\Message\Contracts\MessageFacade;
use Illuminate\Support\Facades\Auth;

class MessageController extends Controller
{
    /**
     * @param StoreRequest $request
     * @param MessageFacade $messageFacade
     * @return array
     */
    public function store(StoreRequest $request, MessageFacade $messageFacade)
    {
        $dto = StoreMessageDto::createFromRequest($request, Auth::user());

        $result = $messageFacade->store($dto);


        return MessageResource::make($result)->resolve();
    }
}
