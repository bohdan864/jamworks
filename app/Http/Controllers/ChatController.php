<?php

namespace App\Http\Controllers;

use App\Dto\Chat\StoreChatDto;
use App\Http\Requests\Chat\StoreRequest;
use App\Http\Resources\Chat\ChatResource;
use App\Http\Resources\Message\MessageResource;
use App\Http\Resources\User\UserResource;
use App\Models\Chat;
use App\Models\User;
use App\Services\Chat\Contracts\ChatService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Inertia\Response;
use Inertia\ResponseFactory;

class ChatController extends Controller
{
    /**
     * @return Response|ResponseFactory
     */
    public function index()
    {
        $users = User::query()->where('id', '!=', auth()->id())->get();
        $users = UserResource::collection($users)->resolve();

        $chats = auth()
            ->user()
            ->chats()
            ->has('messages')
            ->with(['lastMessage'])
            ->withCount('unreadableMessageStatuses')
            ->get();

        $chats = ChatResource::collection($chats)->resolve();

        return inertia('Chat/Index', compact('users', 'chats'));
    }

    /**
     * @param Chat $chat
     * @return JsonResponse|Response|ResponseFactory
     */
    public function show(Chat $chat)
    {
        $page = request('page') ?? 1;

        $users = $chat->getUsers();
        // get messages
        $messages = $chat->getMessagesWithPagination($page);

        $chat->readMessages();

        $isLastPage = $messages->onLastPage();

        $messages = MessageResource::collection($messages)->resolve();

        if ($page > 1) {
            return response()->json([
                'is_last_page' => $isLastPage,
                'messages' => $messages
            ]);
        }

        $users = UserResource::collection($users)->resolve();
        $chat = ChatResource::make($chat)->resolve();

        return inertia('Chat/Show', compact('chat', 'users', 'messages', 'isLastPage'));
    }

    /**
     * @param StoreRequest $request
     * @param ChatService $chatService
     * @return RedirectResponse
     */
    public function store(StoreRequest $request, ChatService $chatService): RedirectResponse
    {
        $dto = StoreChatDto::createFromRequest($request, Auth::user());
        $result = $chatService->store($dto);

        return redirect()->route('chats.show', $result->id);
    }
}
