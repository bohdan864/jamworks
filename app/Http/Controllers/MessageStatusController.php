<?php

namespace App\Http\Controllers;

use App\Dto\MessageStatus\UpdateMessageStatusDto;
use App\Http\Requests\MessageStatus\UpdateRequest;
use App\Services\MessageStatus\Contracts\MessageStatusService;

class MessageStatusController extends Controller
{
    /**
     * @param UpdateRequest $request
     * @param MessageStatusService $messageStatusService
     * @return void
     */
    public function update(UpdateRequest $request, MessageStatusService $messageStatusService): void
    {
        $dto = UpdateMessageStatusDto::createFromRequest($request);

        $messageStatusService->update($dto);
    }
}
