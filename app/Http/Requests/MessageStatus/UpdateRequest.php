<?php

namespace App\Http\Requests\MessageStatus;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'message_id' => 'required|integer|exists:messages,id',
            'user_id' => 'required|integer|exists:users,id'
        ];
    }

    public function authorize(): bool
    {
        return true;
    }
}
