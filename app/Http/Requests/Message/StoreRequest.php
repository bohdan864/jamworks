<?php

namespace App\Http\Requests\Message;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'chat_id' => 'required|integer|exists:chats,id',
            'body' => 'required|string',
            'user_ids' => 'required|array',
            'user_ids.*' => 'required|integer|exists:users,id'
        ];
    }

    public function authorize(): bool
    {
        return true;
    }
}
