<?php

namespace App\Dto\Message;

use App\Http\Requests\Message\StoreRequest;
use App\Models\User;

class StoreMessageDto
{
    private int $chatId;

    private User $user;

    private string $body;

    private array $userIds;

    /**
     * @param int $chatId
     * @return StoreMessageDto
     */
    public function setChatId(int $chatId): self
    {
        $this->chatId = $chatId;
        return $this;
    }

    /**
     * @return int
     */
    public function getChatId(): int
    {
        return $this->chatId;
    }

    /**
     * @param User $user
     * @return StoreMessageDto
     */
    public function setUser(User $user): self
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @return string
     */
    public function getBody(): string
    {
        return $this->body;
    }

    /**
     * @param string $body
     * @return StoreMessageDto
     */
    public function setBody(string $body): self
    {
        $this->body = $body;
        return $this;
    }

    /**
     * @return array
     */
    public function getUserIds(): array
    {
        return $this->userIds;
    }

    /**
     * @param array $userIds
     * @return StoreMessageDto
     */
    public function setUserIds(array $userIds): self
    {
        $this->userIds = $userIds;
        return $this;
    }

    public static function createFromRequest(StoreRequest $request, User $user): StoreMessageDto
    {
        return (new self())
            ->setUser($user)
            ->setBody(htmlspecialchars($request->get('body')))
            ->setChatId($request->get('chat_id'))
            ->setUserIds($request->get('user_ids'));
    }
}
