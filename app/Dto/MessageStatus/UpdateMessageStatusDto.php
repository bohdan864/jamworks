<?php

namespace App\Dto\MessageStatus;

use App\Http\Requests\MessageStatus\UpdateRequest;

class UpdateMessageStatusDto
{
    private int $messageId;

    private int $userId;

    /**
     * @return int
     */
    public function getMessageId(): int
    {
        return $this->messageId;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @param int $messageId
     * @return UpdateMessageStatusDto
     */
    public function setMessageId(int $messageId): self
    {
        $this->messageId = $messageId;
        return $this;
    }

    /**
     * @param int $userId
     * @return UpdateMessageStatusDto
     */
    public function setUserId(int $userId): self
    {
        $this->userId = $userId;
        return $this;
    }

    /**
     * @param UpdateRequest $request
     * @return UpdateMessageStatusDto
     */
    public static function createFromRequest(UpdateRequest $request): UpdateMessageStatusDto
    {
        return (new self())
            ->setMessageId($request->get('message_id'))
            ->setUserId($request->get('user_id'));
    }
}
