<?php

namespace App\Dto\Chat;

use App\Http\Requests\Chat\StoreRequest;
use App\Models\User;

class StoreChatDto
{
    private ?string $title;

    private array $users;

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string|null $title
     * @return StoreChatDto
     */
    public function setTitle(?string $title): self
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return array
     */
    public function getUsers(): array
    {
        return $this->users;
    }

    /**
     * @param array $users
     * @return StoreChatDto
     */
    public function setUsers(array $users): self
    {
        $this->users = $users;
        return $this;
    }

    public static function createFromRequest(StoreRequest $request, User $user): StoreChatDto
    {
        // set users
        $usersIds = array_merge($request->get('users'), [$user->id]);
        sort($usersIds);

        $dto = (new self());

        $dto->setTitle($request->get('title'));
        $dto->setUsers($usersIds);

        return $dto;
    }
}
