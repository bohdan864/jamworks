<?php

namespace App\Exceptions;

use Exception;

class BusinessException extends Exception
{
    /**
     * @var Exception|null
     */
    private Exception|null $exception;

    /**
     * @var string
     */
    private string $userMessage;

    public function __construct(string $userMessage, Exception $exception = null)
    {
        $this->userMessage = $userMessage;
        $this->exception = $exception;

        if ($exception) {
            $this->file = $exception->getFile();
            $this->line = $exception->getLine();
        }

        parent::__construct($this->userMessage);
    }

    /**
     * @return string
     */
    public function getUserMessage(): string
    {
        return $this->userMessage;
    }

    /**
     * @return Exception|null
     */
    public function getException(): ?Exception
    {
        return $this->exception;
    }
}
