<?php

namespace App\Providers;

use App\Services\MessageStatus\BaseMessageStatusService;
use App\Services\MessageStatus\Contracts\MessageStatusService;
use Illuminate\Support\ServiceProvider;

class MessageStatusServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        $this->app->singleton(MessageStatusService::class, BaseMessageStatusService::class);
    }

    public function boot(): void
    {
    }
}
