<?php

namespace App\Providers;

use App\Services\Chat\BaseChatService;
use App\Services\Chat\Contracts\ChatService;
use Illuminate\Support\ServiceProvider;

class ChatServiceServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        $this->app->singleton(ChatService::class, BaseChatService::class);
    }

    public function boot(): void
    {
    }
}
