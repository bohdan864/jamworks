<?php

namespace App\Providers;

use App\Services\Facades\Message\BaseMessageFacade;
use App\Services\Facades\Message\Contracts\MessageFacade;
use App\Services\Message\BaseMessageService;
use App\Services\Message\Contracts\MessageService;
use Illuminate\Support\ServiceProvider;

class MessageServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        $this->app->singleton(MessageService::class, BaseMessageService::class);
        $this->app->singleton(MessageFacade::class, BaseMessageFacade::class);
    }

    public function boot(): void
    {
    }
}
