<?php

namespace App\Services\Facades\Message\Contracts;

use App\Dto\Message\StoreMessageDto;

interface MessageFacade
{
    public function store(StoreMessageDto $dto);
}
