<?php

namespace App\Services\Facades\Message;

use App\Dto\Message\StoreMessageDto;
use App\Events\StoreMessageEvent;
use App\Events\StoreMessageStatusEvent;
use App\Models\Message;
use App\Models\MessageStatus;
use App\Services\Facades\Message\Contracts\MessageFacade;
use App\Services\Message\Contracts\MessageService;
use App\Services\MessageStatus\Contracts\MessageStatusService;
use Exception;
use Illuminate\Support\Facades\DB;
use Throwable;

class BaseMessageFacade implements MessageFacade
{
    public function __construct(
        private readonly MessageService $messageService,
        private readonly MessageStatusService $messageStatusService,
    )
    {

    }


    /**
     * @throws Throwable
     */
    public function store(StoreMessageDto $dto): Message
    {
        try {
            DB::beginTransaction();

            $message = $this->messageService->store($dto);

            // TODO in future add job StoreMessageStatusJob

            foreach ($dto->getUserIds() as $userId) {
                $this->messageStatusService->store($userId, $message);

                $count = MessageStatus::query()->where('chat_id', $message->chat_id)
                    ->where('user_id', $userId)
                    ->where('is_read', false)
                    ->count();

                broadcast(new StoreMessageStatusEvent($count, $message->chat_id, $userId, $message));
            }

            broadcast(new StoreMessageEvent($message))->toOthers();

            DB::commit();

            return $message;
        } catch (Exception|Throwable $e) {
            DB::rollBack();
            throw $e;
        }
    }
}
