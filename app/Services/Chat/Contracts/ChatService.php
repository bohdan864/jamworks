<?php

namespace App\Services\Chat\Contracts;

use App\Dto\Chat\StoreChatDto;
use App\Models\Chat;

interface ChatService
{
    public function store(StoreChatDto $dto): Chat;
}
