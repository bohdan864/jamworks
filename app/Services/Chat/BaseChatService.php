<?php

namespace App\Services\Chat;

use App\Dto\Chat\StoreChatDto;
use App\Exceptions\BusinessException;
use App\Models\Chat;
use App\Services\Chat\Contracts\ChatService;
use Illuminate\Support\Facades\DB;

class BaseChatService implements ChatService
{
    /**
     * Store new chat
     *
     * @throws BusinessException
     */
    public function store(StoreChatDto $dto): Chat
    {
        try {
            return DB::transaction(function () use ($dto) {
                $usersIdString = implode('-', $dto->getUsers());

                $chat = Chat::query()->where('users', $usersIdString)->first();

                if ($chat) {
                    return $this->updateChat($chat, $dto);
                }

                return $this->createChat($dto);
            });
        } catch (\Exception $e) {
            throw new BusinessException('Error storing chat: ' . $e->getMessage());
        }
    }

    /**
     * Create a new chat.
     *
     * @param StoreChatDto $dto
     * @return Chat
     * @throws BusinessException
     */
    private function createChat(StoreChatDto $dto): Chat
    {
        try {
            $chat = new Chat([
                'title' => $dto->getTitle(),
                'users' => implode('-', $dto->getUsers())
            ]);

            if (!$chat->save()) {
                throw new BusinessException('Error creating chat');
            }

            $chat->users()->sync($dto->getUsers());

            return $chat;
        } catch (\Exception $e) {
            throw new BusinessException('Error creating chat: ' . $e->getMessage());
        }
    }


    /**
     * Update an existing chat.
     *
     * @param Chat $chat
     * @param StoreChatDto $dto
     * @return Chat
     * @throws BusinessException
     */
    private function updateChat(Chat $chat, StoreChatDto $dto): Chat
    {
        try {
            $chat->title = $dto->getTitle();

            if (!$chat->save()) {
                throw new BusinessException('Error updating chat');
            }

            $chat->users()->sync($dto->getUsers());

            return $chat;
        } catch (\Exception $e) {
            throw new BusinessException('Error updating chat: ' . $e->getMessage());
        }
    }
}
