<?php

namespace App\Services\MessageStatus;

use App\Dto\MessageStatus\UpdateMessageStatusDto;
use App\Exceptions\BusinessException;
use App\Models\Message;
use App\Models\MessageStatus;
use App\Services\MessageStatus\Contracts\MessageStatusService;

class BaseMessageStatusService implements MessageStatusService
{
    /**
     * @throws BusinessException
     */
    public function store($userId, Message $message): MessageStatus
    {
        try {
            $messageStatus = new MessageStatus();
            $messageStatus->chat_id = $message->chat_id;
            $messageStatus->message_id = $message->id;
            $messageStatus->user_id = $userId;

            if (!$messageStatus->save()) {
                throw new BusinessException('Error creating message status');
            }

            return $messageStatus;
        } catch (\Exception $e) {
            throw new BusinessException('Error storing message status: ' . $e->getMessage());
        }
    }

    /**
     * @param UpdateMessageStatusDto $dto
     * @return bool|int
     */
    public function update(UpdateMessageStatusDto $dto): bool|int
    {
        return MessageStatus::query()->where('user_id', $dto->getUserId())
            ->where('message_id', $dto->getMessageId())
            ->update([
                'is_read' => true
            ]);
    }
}
