<?php

namespace App\Services\MessageStatus\Contracts;

use App\Dto\MessageStatus\UpdateMessageStatusDto;
use App\Models\Message;
use App\Models\MessageStatus;

interface MessageStatusService
{
    public function store($userId, Message $message): MessageStatus;

    public function update(UpdateMessageStatusDto $dto);
}
