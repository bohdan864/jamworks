<?php

namespace App\Services\Message;

use App\Dto\Message\StoreMessageDto;
use App\Exceptions\BusinessException;
use App\Models\Message;
use App\Services\Message\Contracts\MessageService;

class BaseMessageService implements MessageService
{
    /**
     * @throws BusinessException
     */
    public function store(StoreMessageDto $dto): Message
    {
        try {
            $message = new Message();
            $message->chat_id = $dto->getChatId();
            $message->user_id = $dto->getUser()->id;
            $message->body = $dto->getBody();

            if (!$message->save()) {
                throw new BusinessException('Error creating message');
            }

            return $message;
        } catch (\Exception $e) {
            throw new BusinessException('Error storing message: ' . $e->getMessage());
        }
    }
}
