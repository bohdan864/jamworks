<?php

namespace App\Services\Message\Contracts;

use App\Dto\Message\StoreMessageDto;
use App\Models\Message;

interface MessageService
{
    public function store(StoreMessageDto $dto): Message;
}
