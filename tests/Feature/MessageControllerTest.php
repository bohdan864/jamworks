<?php

namespace Tests\Feature;

use App\Dto\Message\StoreMessageDto;
use App\Http\Controllers\MessageController;
use App\Http\Requests\Message\StoreRequest;
use App\Http\Resources\Message\MessageResource;
use App\Models\User;
use App\Services\Facades\Message\Contracts\MessageFacade;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use PHPUnit\Framework\MockObject\Exception;
use Tests\TestCase;

class MessageControllerTest extends TestCase
{
    use RefreshDatabase;

    use RefreshDatabase; // Clears the database after each test
    use WithFaker; // Provides methods for creating fake data


    /** @test
     * @throws Exception
     */
    public function it_stores_message_correctly(): void
    {
        // Create a stub for the MessageFacade facade
        $messageFacade = $this->createMock(MessageFacade::class);

        // Create a controller instance
        $controller = new MessageController();

        // Generate fake data for the request
        $request = new StoreRequest([
            'chat_id' => $this->faker->numberBetween(1, 100),
            'body' => $this->faker->sentence,
            'user_ids' => [$this->faker->numberBetween(1, 100)]
        ]);

        $user = User::factory()->create();
        // Authorize the test user
        $this->actingAs($user);
        // Create a DTO based on the request
        $dto = StoreMessageDto::createFromRequest($request, $user);
        // We expect that the store method of the facade will be called with the DTO data
        $messageFacade->expects($this->once())
            ->method('store')
            ->with($dto);

        // Call the controller method
        $response = $controller->store($request, $messageFacade);

        // Check that the method returns a MessageResource instance
        $this->assertInstanceOf(MessageResource::class, $response);
    }
}
