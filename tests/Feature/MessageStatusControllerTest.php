<?php

namespace Tests\Feature;

use App\Dto\MessageStatus\UpdateMessageStatusDto;
use App\Http\Controllers\MessageStatusController;
use App\Http\Requests\MessageStatus\UpdateRequest;
use App\Services\MessageStatus\Contracts\MessageStatusService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use PHPUnit\Framework\MockObject\Exception;
use Tests\TestCase;


class MessageStatusControllerTest extends TestCase
{
    use RefreshDatabase; // Clears the database after each test
    use WithFaker; // Provides methods for creating fake data

    /** @test
     * @throws Exception
     */
    public function it_updates_message_status_correctly(): void
    {
        // Create a stub for the MessageStatusService service
        $messageStatusService = $this->createMock(MessageStatusService::class);

        // Create a controller instance
        $controller = new MessageStatusController();

        // Generate fake data for the request
        $request = new UpdateRequest([
            'message_id' => $this->faker->numberBetween(1, 100),
            'user_id' => $this->faker->numberBetween(1, 100)
        ]);

        // Create a DTO based on the request
        $dto = UpdateMessageStatusDto::createFromRequest($request);

        // We expect that the service's update method will be called with the DTO data
        $messageStatusService->expects($this->once())
            ->method('update')
            ->with($dto);

        // Call the controller method
        $controller->update($request, $messageStatusService);
    }
}
