<?php

namespace Tests\Feature;

use App\Models\Chat;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ChatControllerTest extends TestCase
{
    use RefreshDatabase;

    public function testIndex(): void
    {
        // Create a test user
        $user = User::factory()->create();
        // Authorize the test user
        $this->actingAs($user);
        // Create test chats
        Chat::factory()->count(3)->create();
        // Call the index method
        $response = $this->get(route('chats.index'));
        // Check that the page opens successfully
        $response->assertStatus(200);

        $this->assertDatabaseCount('chats', 3);
    }

    /** @test */
    public function attribute_title_is_not_required()
    {
        // data with empty title
        $data = [
            'title' => '',
        ];

        $res = $this->post('/chats', $data);

        $res->assertRedirect();
        $res->assertValid('title');
    }
}
